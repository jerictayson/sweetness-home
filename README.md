## Sweetness Home Ordering System

**This project is a type of ordering system for cake and bakery businesses**

This is just a hobby project and may be unuseful to someone.

Feel free to use it and happy coding!.

**Features**

* Track Orders
* Create new Orders
* Create Accounts
* Log history
* Monthly Reports
* And much more.

**Software Requirements:**

In order to run this application you need the following:

* [XAMPP](https://www.apachefriends.org/download.html)
* A Computer.

**Attribution**

Unsplash - For default images
