-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 25, 2023 at 03:15 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET
SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET
time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `OrderingSystem`
--
CREATE
DATABASE IF NOT EXISTS `OrderingSystem` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE
`OrderingSystem`;

-- --------------------------------------------------------

--
-- Table structure for table `Cart`
--

CREATE TABLE `Cart`
(
	`CartID`    int(11) NOT NULL,
	`ProductID` int(11) NOT NULL,
	`Quantity`  int(11) NOT NULL,
	`UserID`    int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `Cart`
--

INSERT INTO `Cart` (`CartID`, `ProductID`, `Quantity`, `UserID`)
VALUES (1, 1, 1, 6),
	   (2, 4, 1, 6),
	   (3, 7, 1, 6),
	   (4, 8, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `HistoryLog`
--

CREATE TABLE `HistoryLog`
(
	`LogID`   int(11) NOT NULL,
	`UserID`  int(11) NOT NULL,
	`LogType` varchar(100) NOT NULL,
	`Reason`  text         NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `OrderDetails`
--

CREATE TABLE `OrderDetails`
(
	`OrderID`    int(11) NOT NULL,
	`CartID`     int(11) NOT NULL,
	`Quantity`   int(11) NOT NULL,
	`TotalPrice` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders`
(
	`OrderID`      int(11) NOT NULL,
	`UserID`       int(11) NOT NULL,
	`DateCreated`  date NOT NULL DEFAULT current_timestamp(),
	`ItemReceived` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Product`
--

CREATE TABLE `Product`
(
	`ProductID`   int(11) NOT NULL,
	`ProductName` varchar(255) NOT NULL,
	`Flavor`      varchar(100) NOT NULL,
	`Price` double NOT NULL,
	`Stocks`      int(11) NOT NULL,
	`Description` text         NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `Product`
--

INSERT INTO `Product` (`ProductID`, `ProductName`, `Flavor`, `Price`, `Stocks`, `Description`)
VALUES (1, 'Rich Velvet Chocolate Symphony', 'Chocolate', 1000, 100, 'No Description Provided.'),
	   (3, 'Sweet Scarlet Strawberry Pie', 'Strawberry', 1000, 100, 'No Description Provided.'),
	   (4, 'Vanilla Mango Chocolate Temptation', 'Vanilla Mango', 1000, 100, 'No Description Provided.'),
	   (5, 'Sweet Mocha Strawberry Cake', 'Mocha Strawberry', 1000, 100, 'No Description Provided.'),
	   (6, 'Vanilla Mango Paradise Cake', 'Vanilla Mango', 1000, 100, 'No Description Provided.'),
	   (7, 'Berry Patch Vanilla Dream Cake', 'Vaniila', 1000, 100, 'No Description Provided.'),
	   (8, 'Chocolate Milky Way Cake', 'Chocolate', 1000, 100, 'No Description Provided.'),
	   (9, 'Chocolate Drizzled Milk Cake', 'Milk Chocolate', 1000, 100, 'No Description Provided.'),
	   (10, 'Decadent Chocolate Cream Supreme', 'Brown Chocolate', 1000, 100, 'No Description Provided.');

-- --------------------------------------------------------

--
-- Table structure for table `ProductImage`
--

CREATE TABLE `ProductImage`
(
	`ProductImageID` int(11) NOT NULL,
	`ImagePath`      varchar(255) NOT NULL,
	`ProductID`      int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `ProductImage`
--

INSERT INTO `ProductImage` (`ProductImageID`, `ImagePath`, `ProductID`)
VALUES (1, 'Product-1.jpg', 1),
	   (3, 'Product-2.jpg', 3),
	   (4, 'Product-3.jpg', 4),
	   (5, 'Product-4.jpg', 5),
	   (6, 'Product-5.jpg', 6),
	   (7, 'Product-6.jpg', 7),
	   (8, 'Product-7.jpg', 8),
	   (9, 'Product-8.jpg', 9),
	   (10, 'Product-9.jpg', 10);

-- --------------------------------------------------------

--
-- Table structure for table `UserAccount`
--

CREATE TABLE `UserAccount`
(
	`UserID`      int(11) NOT NULL,
	`Email`       varchar(50)  NOT NULL,
	`Password`    varchar(70)  NOT NULL,
	`FullName`    varchar(255) NOT NULL,
	`Address`     varchar(255) NOT NULL,
	`PhoneNumber` varchar(100) NOT NULL,
	`BirthDate`   date         NOT NULL DEFAULT current_timestamp(),
	`AccountType` varchar(100) NOT NULL DEFAULT 'CUSTOMER'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `UserAccount`
--

INSERT INTO `UserAccount` (`UserID`, `Email`, `Password`, `FullName`, `Address`, `PhoneNumber`, `BirthDate`,
						   `AccountType`)
VALUES (5, 'jeric_tayson19@tutanota.com', '$2y$10$zsi3p99fkBpMryV6Oug59OQATWYOAHwEG9gO4zmbjkA5hWYUmzuTO',
		'Jericson Tayson', 'Hagonoy Bulacan', '09084867370', '2023-05-17', 'CUSTOMER'),
	   (6, 'clarence_jane@jdl.com', '$2y$10$hv8RLDMUSH1IURc8nFApBekwvGRhCd1jmM9wIzOZKG7mojq8PpA5i',
		'Clarence Jane De Leon', 'Malolos Bulacan', '09974425627', '2023-05-25', 'CUSTOMER'),
	   (7, 'jay@yahoo.com', '$2y$10$NOxvqAzuX8134/IZb2KZ.eRbQUecF/calLa2bGzMDKGwYqLPWc4XC', 'Jay Jay',
		'Malolos Bulacan', '09084867370', '2023-05-15', 'CUSTOMER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Cart`
--
ALTER TABLE `Cart`
	ADD PRIMARY KEY (`CartID`),
  ADD KEY `fk_UserID` (`UserID`),
  ADD KEY `fk_CartProductID` (`ProductID`);

--
-- Indexes for table `HistoryLog`
--
ALTER TABLE `HistoryLog`
	ADD PRIMARY KEY (`LogID`),
  ADD KEY `fk_LogIDUserID` (`UserID`);

--
-- Indexes for table `OrderDetails`
--
ALTER TABLE `OrderDetails`
	ADD PRIMARY KEY (`OrderID`, `CartID`),
  ADD KEY `fk_OrderDetailsCartID` (`CartID`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
	ADD PRIMARY KEY (`OrderID`),
  ADD KEY `fk_OrderUserID` (`UserID`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
	ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `ProductImage`
--
ALTER TABLE `ProductImage`
	ADD PRIMARY KEY (`ProductImageID`),
  ADD KEY `fk_ProductID` (`ProductID`);

--
-- Indexes for table `UserAccount`
--
ALTER TABLE `UserAccount`
	ADD PRIMARY KEY (`UserID`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Cart`
--
ALTER TABLE `Cart`
	MODIFY `CartID` int (11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `HistoryLog`
--
ALTER TABLE `HistoryLog`
	MODIFY `LogID` int (11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Orders`
--
ALTER TABLE `Orders`
	MODIFY `OrderID` int (11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Product`
--
ALTER TABLE `Product`
	MODIFY `ProductID` int (11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ProductImage`
--
ALTER TABLE `ProductImage`
	MODIFY `ProductImageID` int (11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `UserAccount`
--
ALTER TABLE `UserAccount`
	MODIFY `UserID` int (11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Cart`
--
ALTER TABLE `Cart`
	ADD CONSTRAINT `fk_CartProductID` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ProductID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_UserID` FOREIGN KEY (`UserID`) REFERENCES `UserAccount` (`UserID`) ON
DELETE
NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `HistoryLog`
--
ALTER TABLE `HistoryLog`
	ADD CONSTRAINT `fk_LogIDUserID` FOREIGN KEY (`UserID`) REFERENCES `UserAccount` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `OrderDetails`
--
ALTER TABLE `OrderDetails`
	ADD CONSTRAINT `fk_OrderDetailsCartID` FOREIGN KEY (`CartID`) REFERENCES `Cart` (`CartID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_OrderDetailsOrderID` FOREIGN KEY (`OrderID`) REFERENCES `Orders` (`OrderID`) ON
DELETE
NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Orders`
--
ALTER TABLE `Orders`
	ADD CONSTRAINT `fk_OrderUserID` FOREIGN KEY (`UserID`) REFERENCES `UserAccount` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ProductImage`
--
ALTER TABLE `ProductImage`
	ADD CONSTRAINT `fk_ProductID` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ProductID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
