$(document).ready(() => {
	load();
});

function addToCart(id, quantity) {
	if (quantity == undefined) quantity = 1;
	$.ajax({
		url: `${window.location.href}/addToCart`,
		type: "POST",
		data: {
			id: id,
			quantity: quantity,
		},
		success: function (res) {
			if (res == 1) {
				Swal.fire({
					position: "top-right",
					icon: "success",
					width: "300px",
					title: "Product Added to Cart",
					showConfirmButton: false,
					timer: 2000,
				});

				load();
			}
		},
		error: function (xhr, textStatus, errorThrown) {
			if (xhr.status === 401) {
				let link = new URL(window.location.href).origin;
				window.location.href = `${link}/sweetness-home/Login`;
			}
		},
	});
}

function load() {
	$.get(`${window.location.href}/loadCartCount`, function (data, res) {
		loadCartCount(data);
	});
}

function loadCartCount(data) {
	$("#cart-count").text('');
	$("#cart-count").text(data);
}
