$(document).ready(function () {

	loadCarts();

});

function loadCarts() {

	console.log(window.location.origin);
	$.get(`${window.location.href}/loadCarts`, function (data) {

		data = JSON.parse(data);
		data.forEach(cart => {

			let cartContainer = document.createElement('div');
			let detailContainer = document.createElement('div');
			let img = document.createElement('img');
			let title = document.createElement('h5');
			let price = document.createElement('p');
			let quantity = document.createElement('p');

			let checkBox = document.createElement("input");
			checkBox.type = 'checkbox';
			checkBox.classList.add('form-check-input');
			checkBox.style.marginRight = '20px';
			checkBox.style.cursor = 'pointer';
			checkBox.setAttribute('value', data.ProductID);
			cartContainer.classList.add('cart-window');
			img.setAttribute('src', `images/productimages/${cart.ImagePath}`);
			img.classList.add('cart-img')
			title.innerText = cart.ProductName;
			price.style.margin = '0px';
			price.innerText = `Price: ${cart.Price}`;
			quantity.innerText = `Quantity: ${cart.Quantity}`;
			cartContainer.append(checkBox);
			cartContainer.append(img);
			detailContainer.append(title);
			detailContainer.append(price);
			detailContainer.append(quantity);
			detailContainer.style.marginLeft = '10px';
			cartContainer.append(detailContainer);
			$('#main').append(cartContainer);

		})

	});
}
