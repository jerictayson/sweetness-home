<?php

/**
 * @property $session
 */
class Cart extends CI_Controller
{


	public function index()
	{

		$isLoggedIn = $this->session->userdata('email');

		if ($isLoggedIn == null) {

			$data = [];
			$data['error'] = 'You must login to continue.';
			$data['display'] = 'block';
			$this->load->view('login', $data);

		} else {

			$data['isLoggedIn'] = $isLoggedIn;
			$this->load->view('cart', $data);
		}

	}

	function loadCarts()
	{

		$this->load->model('Account');
		$email = $this->session->userdata('email');
		echo json_encode($this->Account->getCarts($email));
	}

}
