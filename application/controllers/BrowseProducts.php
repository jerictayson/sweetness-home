<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property $session
 * @property $Product
 * @property $input
 * @property $UserAccount
 * @property $Account
 * @property $output
 */
class BrowseProducts extends CI_Controller
{

	public function index()
	{

		$isLoggedIn = $this->session->userdata('email');
		$data['isLoggedIn'] = $isLoggedIn;

		$this->load->model('Product');

		$products = $this->Product->getData();
		$data['products'] = $products;

		$this->load->view('browse_products', $data);
	}

	public function addToCart()
	{

		$email = $this->session->userdata('email');
		$output = '';
		if ($email != NULL) {

			$productID = trim($this->input->post('id'));
			$quantity = trim($this->input->post('quantity'));

			$this->load->model('UserAccount');
			$account = $this->UserAccount->search($email);

			if ($account != NULL) {
				$this->load->model('Account');

				$cart = [];
				$cart['ProductID'] = $productID;
				$cart['Quantity'] = $quantity;
				$cart['UserID'] = $account->UserID;

				$res = $this->Account->addToCart($cart, $email);

				$output = $this->output->set_content_type('application/json')->set_status_header(201)->set_output($res);
			} else {

				$output = $this->output->set_content_type('application/json')->set_status_header(401);
			}

		} else {

			$output = $this->output->set_content_type('application/json')->set_status_header(401);
		}

		return $output;
	}

	public function loadCartCount()
	{


		$email = $this->session->userdata('email');
		$output = '0';
		if ($email != null) {

			$this->load->model('Account');
			$output = $this->Account->getCartCount($email);
		}

		echo $output;
	}
}
