<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property $session
 * @property $form_validation
 * @property $input
 * @property $UserAccount
 */
class Login extends CI_Controller
{

	private $data = [];

	public function index()
	{

		$this->data['error'] = '';
		$this->data['display'] = 'none';
		if ($this->session->userdata('email') == NULL)
			$this->load->view('login', $this->data);
		else
			redirect('Welcome/index');
	}

	public function login()
	{

		$this->data['error'] = '';
		$this->data['display'] = 'none';
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if (!$this->form_validation->run()) {

			$this->load->view('login', $this->data);
		} else {
			$email = trim($this->input->post('email'));
			$password = trim($this->input->post('password'));
			$this->load->model('UserAccount');
			if ($this->UserAccount->login($email, $password)) {
				$this->session->set_userdata('email', $email);
				redirect('Welcome/index');
			} else {
				$this->load->view('login', $this->data);
			}
		}
	}

	public function logout()
	{

		$this->session->unset_userdata('email');
		redirect('Welcome/index');
	}
}
