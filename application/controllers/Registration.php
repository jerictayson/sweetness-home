<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property $input
 * @property $UserAccount
 * @property $phpmailer_lib
 * @property $form_validation
 */
class Registration extends CI_Controller
{
	private $message = [];

	public function index()
	{
		$this->message['error'] = '';
		$this->message['display'] = 'none';
		$this->load->view('registration', $this->message);
	}

	public function register()
	{
		$this->message['error'] = '';
		if (!$this->isValid()) {
			$this->load->view('registration', $this->message);
		} else {

			$this->load->model('UserAccount');

			if ($this->UserAccount->search($this->processData()['Email']) == NULL) {

				$uuid = $this->UserAccount->insert($this->processData());
				$this->sendEmail($this->processData()['Email'], $this->processData()['FullName'], $uuid);
			} else {

				$this->message['error'] = 'This email is already registered. Please try again.';
				$this->message['display'] = 'block';
				$this->load->view('registration', $this->message);
			}

		}
	}

	public function verifyEmail($email, $token)
	{


		if ($token == null) {

			echo 'Invalid call.';
		} else {
			$this->load->model('UserAccount');
			$result = $this->UserAccount->verifyToken($token, $email);
			echo $result;
		}
	}


	private function sendEmail($email, $fullName, $token)
	{

		$this->load->library('PhpMailer_lib');

		$mail = $this->phpmailer_lib->load();

		// SMTP configuration
		$mail->isSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->SMTPAuth = true;
		$mail->Username = getenv('EMAIL');
		$mail->Password = getenv('EMAILTOKEN');
		$mail->SMTPSecure = 'ssl';
		$mail->Port = 465;

		// Add a recipient
		$mail->addAddress($email);

		// Email subject
		$mail->Subject = 'Verification';

		// Set email format to HTML
		$mail->isHTML(true);

		$link = base_url() . "Registration/verifyEmail/$email/$token";
		// Email body content
		$mailContent = "<h1>Hello {$fullName}!</h1>
            <p>Please click the link to verify your account: 
            <a href='{$link}'>Click Here!</a></p>";
		$mail->Body = $mailContent;

		// Send email
		if (!$mail->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			$this->load->view('emailsent');
		}
	}

	private function processData()
	{

		$firstname = trim($this->input->post('firstname'));
		$lastname = trim($this->input->post('lastname'));
		$address = trim($this->input->post('address'));
		$birthdate = trim($this->input->post('birthdate'));
		$email = trim($this->input->post('email'));
		$password = trim($this->input->post('password'));
		$password = password_hash($password, PASSWORD_BCRYPT);
		$phonenumber = trim($this->input->post('phonenumber'));

		$newUser = array(
			'FullName' => $firstname . ' ' . $lastname,
			'Address' => $address,
			'BirthDate' => $birthdate,
			'Email' => $email,
			'Password' => $password,
			'PhoneNumber' => $phonenumber
		);
		return $newUser;
	}

	private function isValid()
	{
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('address', 'First Name', 'trim|required');
		$this->form_validation->set_rules('birthdate', 'Birth Date', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('phonenumber', 'Phone Number', 'trim|required|numeric');
		$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'trim|required|matches[password]');

		return $this->form_validation->run();
	}
}
