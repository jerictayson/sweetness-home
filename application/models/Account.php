<?php

class Account extends CI_Model
{

	public function getCartCount($email)
	{

		$this->db->where('Email', $email);
		$res = $this->db->get('UserAccount');

		$acc = $res->row();
		$uid = $acc->UserID;

		$this->db->where('UserID', $uid);
		$res = $this->db->get('Cart');

		return count($res->result());
	}

	public function addToCart($cart, $email)
	{

		$this->db->where('Email', $email);
		$res = $this->db->get('UserAccount');

		$acc = $res->row();
		$uid = $acc->UserID;

		$condition = array('ProductID' => $cart['ProductID'], 'UserID' => $uid);

		$this->db->where($condition);
		$res = $this->db->get('Cart');

		if (count($res->result()) > 0) {

			$this->db->where($condition);
			$res = $this->db->get('Cart');

			$this->db->set('Quantity', $res->row()->Quantity + 1);
			$this->db->where($condition);

			$this->db->update('Cart');
			return 1;
		}

		return $this->db->insert('Cart', $cart);
	}

	public function loadCart($email)
	{

		$this->db->where('Email', $email);
		$res = $this->db->get('UserAccount');

		$acc = $res->row();
		$uid = $acc->UserID;
	}

	public function getCarts($email)
	{

		$this->db->where('Email', $email);
		$res = $this->db->get('UserAccount');

		$acc = $res->row();
		$uid = $acc->UserID;

		$carts = [];
		$this->db->where('UserID', $uid);
		$res = $this->db->get('Cart');

		foreach ($res->result() as $row) {


			$cart = [];
			$cart['CartID'] = $row->CartID;
			$cart['Quantity'] = $row->Quantity;

			$this->db->where('ProductID', $row->ProductID);
			$this->db->limit(1);
			$query = $this->db->get('ProductImage');
			$row = $query->row();
			$cart['ImagePath'] = $row->ImagePath;

			$this->db->where('ProductID', $row->ProductID);
			$this->db->limit(1);
			$query = $this->db->get('Product');
			$row = $query->row();
			$cart['ProductName'] = $row->ProductName;
			$cart['Price'] = $row->Price;
			array_push($carts, $cart);

		}

		return $carts;
	}
}
