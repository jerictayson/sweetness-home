<?php

class Product extends CI_Model
{

	public function getData()
	{

		$products = $this->db->get('Product');
		$fetched_products = array();

		if ($products->num_rows() > 0) {

			foreach ($products->result() as $row) {

				$product = [];
				$product['ProductID'] = $row->ProductID;
				$product['ProductName'] = $row->ProductName;
				$product['Flavor'] = $row->Flavor;
				$product['Price'] = $row->Price;
				$product['Stocks'] = $row->Stocks;
				$product['Description'] = $row->Description;

				$this->db->where('ProductID', $product['ProductID']);
				$images = $this->db->get('ProductImage');

				$productImages = array();
				if ($images->num_rows() > 0) {

					foreach ($images->result() as $image) {

						array_push($productImages, $image->ImagePath);
					}
				}
				$product['ImagePath'] = $productImages;
				array_push($fetched_products, $product);
			}
		}

		return $fetched_products;
	}

}
