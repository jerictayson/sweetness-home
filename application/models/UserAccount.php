<?php

/**
 * @property $db
 */
class UserAccount extends CI_Model
{

	public function insert($data)
	{

		$this->db->insert('UserAccount', $data);
		$this->db->order_by('UserID', 'DESC');
		$query = $this->db->get('UserAccount', 1);
		$data = $query->row();
		$id = $data->UserID;

		$currentDateTime = date('Y-m-d H:i:s');
		$newDateTime = date('Y-m-d H:i:s', strtotime($currentDateTime . '+5 minutes'));//Add remaining minutes to verify.

		$this->load->library('uuid');
		$token = [];
		$token['UUID'] = $this->uuid->v4();
		$token['ExpirationDate'] = $newDateTime;
		$token['IsUsed'] = 0;
		$token['UserID'] = $id;
		$this->db->insert('UserToken', $token);

		//fetching the latest record to return the token id for the email link
		$this->db->order_by('ID', 'DESC');
		$query = $this->db->get('UserToken', 1);
		$data = $query->row();
		return $data->UUID;
	}


	/**
	 * @throws Exception
	 */
	public function verifyToken($token, $email)
	{

		$uid = $this->search($email);

		if ($uid == null) {

			return "Account not found";
		} else {

			$condition = array('UUID' => $token, 'UserID' => $uid->UserID);
			$this->db->where($condition);
			$query = $this->db->get('UserToken', 1);
			$row = $query->row();

			if ($row->IsUsed == 0) {
				$exprDate = $row->ExpirationDate;
				$currentDateTime = date('Y-m-d H:i:s');

				$currentDateTime = new DateTime($currentDateTime);
				$exprDate = new DateTime($exprDate);

				if ($currentDateTime < $exprDate) {

					$this->db->set('IsUsed', 1);
					$this->db->where($condition);
					$this->db->update('UserToken');
					$this->verifyAccount($email);
					return "Account Verified";
				} else
					return "Token is expired";
			} else {
				return "Token is already used";
			}


		}
	}

	public function verifyAccount($email)
	{

		$this->db->set('IsVerified', 1);
		$this->db->where('Email', $email);
		$this->db->update('UserAccount');
	}

	public function login($email, $password)
	{

		$this->db->where('Email', $email);
		$data = $this->db->get('UserAccount');

		if ($data->num_rows() > 0) {

			$record = $data->row();
			return password_verify($password, $record->Password);
		}
	}

	public function search($email)
	{

		$this->db->where('email', $email);
		$data = $this->db->get('UserAccount');
		if ($data->num_rows() > 0) {

			return $data->row();
		} else {
			return null;
		}
	}
}
