<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<script src="https://kit.fontawesome.com/ecde828da2.js" crossorigin="anonymous"></script>
	<title>Sweetness Home - Your best catering service</title>
	<style>
		.hero {

			background-image: url('<?php echo base_url() ?>assets/images/hero.jpg');
			background-repeat: no-repeat;
			background-position: center;
			background-size: cover;
			min-height: 80vh;

		}
	</style>
</head>

<body>
<div class="container-fluid" id="header">
	<div class="container-md">
		<nav class="navbar navbar-expand-lg">
			<div class="container">
				<a href="<?php echo base_url() ?>" class="navbar-brand text-white" id="title">
					Sweetness Home
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
						data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
						aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>BrowseProducts">Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#">About Us</a>
						</li>
					</ul>

					<ul class="navbar-nav ms-auto">
						<li class="nav-item">
							<?php
							if ($isLoggedIn == '') {
								?>
								<a class="nav-link text-white me-3" href="<?php echo base_url() ?>Login/">
									Login
								</a>
							<?php } else { ?>
								<a class="nav-link text-white me-3" href="#">
									<i class="fa-solid fa-user fa-xl text-white"></i>
									Profile</a>
								<?php
							}
							?>
						</li>

						<li class="nav-item">
							<?php
							if ($isLoggedIn == '') {
								?>
								<a class="nav-link text-white" href="<?php echo base_url() ?>Registration">Sign Up</a>
							<?php } else { ?>

								<a class="nav-link text-white" href="<?php echo base_url() ?>Login/logout">Logout</a>
							<?php } ?>

						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>
<div class="container-fluid p-0">
	<div class="hero d-flex row align-items-center mb-3">
		<div class="col-3 ms-5">
			<p class="hero-title">Satisfy your Cravings.</p>
			<a href="" class="btn btn-primary p-3" style="font-weight: 600;">Browse Shop.</a>
		</div>
	</div>

	<div class="container">
		<h1 class="text-center section-title">What we Offer</h1>
		<div class="row justify-content-between">
			<div class="col">
				<div class="card">
					<img src="<?php echo base_url() ?>assets/images/product.jpg" class="card-img-top" alt="Card Image">
					<div class="card-body">
						<h5 class="card-title">Cake is an art</h5>
						<p class="card-text">A trendy cake store that serves Instagram-worthy dessert creations,
							featuring trendy flavors like matcha, salted caramel, and lavender, topped with edible
							flowers and artistic brushstrokes, making every bite a moment to capture and savor.</p>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="card">
					<img src="<?php echo base_url() ?>assets/images/product.jpg" class="card-img-top" alt="Card Image">
					<div class="card-body">
						<h5 class="card-title">Customer is Always Priority</h5>
						<p class="card-text"> customer-centric cake store that offers convenient online ordering and
							delivery services, allowing customers to effortlessly enjoy scrumptious cakes delivered
							right to their doorstep for any special occasion or spontaneous craving.</p>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="card">
					<img src="<?php echo base_url() ?>assets/images/product.jpg" class="card-img-top" alt="Card Image">
					<div class="card-body">
						<h5 class="card-title">Experience a delightful taste</h5>
						<p class="card-text">A delightful cake store that provides personalized cake design
							consultations, ensuring that every customer's vision is brought to life with meticulous
							attention to detail and a sprinkle of magic.</p>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>

</body>

</html>
