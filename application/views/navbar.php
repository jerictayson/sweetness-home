<div class="container-fluid" id="header">
	<div class="container-md">
		<nav class="navbar navbar-expand-lg">
			<div class="container">
				<a href="" class="navbar-brand text-white" id="title">
					Sweetness Home
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
						data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
						aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link text-white" href="#">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#">Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#">About Us</a>
						</li>
					</ul>

					<ul class="navbar-nav ms-auto">
						<li class="nav-item">
							<a class="nav-link text-white" href="#">Login</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#">Sign Up</a>
						</li>
					</ul>


				</div>
			</div>
		</nav>
	</div>
</div>
