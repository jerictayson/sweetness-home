<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<script defer src="<?php echo base_url() ?>assets/js/jquery-3.7.0.min.js"></script>
	<script defer src="<?php echo base_url() ?>assets/js/functions/main.js"></script>
	<script defer src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="https://kit.fontawesome.com/ecde828da2.js" crossorigin="anonymous"></script>
	<title>Browse Products</title>
</head>

<body>
<div class="container-fluid" id="header">
	<div class="container-md">
		<nav class="navbar navbar-expand-lg">
			<div class="container">
				<a href="<?php echo base_url() ?>" class="navbar-brand text-white" id="title">
					Sweetness Home
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
						data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
						aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>BrowseProducts">Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#">About Us</a>
						</li>
					</ul>

					<ul class="navbar-nav ms-auto">
						<?php
						if ($isLoggedIn == '') {
							?>

							<li class="nav-item">
								<a class="nav-link text-white" href="<?php echo base_url() ?>Login">Login</a>
							</li>
						<?php } else { ?>
							<li class='nav-item'>
								<a class="nav-link text-white" href="#">
									<i class="fa-solid fa-cart-shopping text-white p-1"
									   style='border: 1px solid white; border-radius: 10px;'>
										<span class='badge' id='cart-count'></span>
									</i>
								</a>
							</li>

							<li class='nav-item'>
								<a class="nav-link text-white" href="#">Profile</a>
							</li>
							<?php
						}
						?>

						<li class="nav-item">
							<?php
							if ($isLoggedIn == '') {
								?>
								<a class="nav-link text-white" href="<?php echo base_url() ?>Registration">Sign Up</a>
							<?php } else { ?>

								<a class="nav-link text-white" href="<?php echo base_url() ?>Login/logout">Logout</a>
							<?php } ?>

						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>

<div class="container-fluid mt-5">

	<div class="container-md">
		<h1 id="title" class="mt-5 mb-5">Browse Shop</h1>

		<div class="row">

			<?php
			$string = '';
			foreach ($products as $product) { ?>
				<div class="col-md-4 mb-3">
					<div class="card" style="width: 22rem;">
						<img src="images/productimages/<?php echo $product['ImagePath'][0] ?>" class="card-img-top"
							 alt="...">
						<div class="card-body">
							<h5 class="card-title text-muted"><?php echo $product['ProductName'] ?></h5>
							<h6 class="card-subtitle text-muted mb-2">Price: <?php echo $product['Price'] ?></h6>
							<h6 class="card-subtitle text-muted">Stocks: <?php echo $product['Stocks'] ?></h6>
							<p class="card-text"><?php echo $product['Description'] ?></p>
							<a href="" class="btn btn-success me-2">
								<i class="fa-solid text-white fa-cart-shopping"></i>
								Buy Now</a>
							<button class="btn btn-primary" onclick="addToCart(
							<?php echo $product['ProductID'] ?>)">
								<i class="fa-regular fa-heart text-white"></i>
								Add to cart
							</button>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>

</div>
</body>

</html>
