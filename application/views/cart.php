<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
	<script defer src="<?php echo base_url() ?>assets/js/jquery-3.7.0.min.js"></script>
	<script defer src="<?php echo base_url() ?>assets/js/functions/loadcart.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<script src="https://kit.fontawesome.com/ecde828da2.js" crossorigin="anonymous"></script>
	<title>Sweetness Home - Your best catering service</title>
	<style>
		.hero {

			background-image: url('<?php echo base_url() ?>assets/images/hero.jpg');
			background-repeat: no-repeat;
			background-position: center;
			background-size: cover;
			min-height: 80vh;

		}
	</style>
</head>

<body style="overflow-x: hidden">
<div class="container-fluid" id="header">
	<div class="container-md">
		<nav class="navbar navbar-expand-lg">
			<div class="container">
				<a href="<?php echo base_url() ?>" class="navbar-brand text-white" id="title">
					Sweetness Home
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
						data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
						aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>BrowseProducts">Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#">About Us</a>
						</li>
					</ul>

					<ul class="navbar-nav ms-auto">
						<li class="nav-item">
							<?php
							if ($isLoggedIn == '') {
								?>
								<a class="nav-link text-white me-3" href="<?php echo base_url() ?>Login/">
									Login
								</a>
							<?php } else { ?>
								<a class="nav-link text-white me-3" href="#">
									<i class="fa-solid fa-user fa-xl text-white"></i>
									Profile</a>
								<?php
							}
							?>
						</li>

						<li class="nav-item">
							<?php
							if ($isLoggedIn == '') {
								?>
								<a class="nav-link text-white" href="<?php echo base_url() ?>Registration">Sign Up</a>
							<?php } else { ?>

								<a class="nav-link text-white" href="<?php echo base_url() ?>Login/logout">Logout</a>
							<?php } ?>

						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>

<div class="row">


	<div class="col-2 justify-content-center align-items-center">
	</div>
	<div class="col-8 p-2 min-vh-100">

		<h1 class="mb-5 mt-5" id="title">Your Cart</h1>
		<div id="main" class="align-items-center mt-5">
		</div>

		<div class="justify-content-end d-flex mt-3">
			<button class="btn btn-primary fw-bolder p-2 pt-3 pb-3">Proceed to Checkout</button>
		</div>

	</div>
	<div class="col-2">

	</div>
</div>
<div class="row justify-content-end"></div>
</body>
</html>
