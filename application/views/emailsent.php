<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<script defer src="<?php echo base_url() ?>assets/js/jquery-3.7.0.min.js"></script>
	<script defer src="<?php echo base_url() ?>assets/js/functions/main.js"></script>
	<script defer src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="https://kit.fontawesome.com/ecde828da2.js" crossorigin="anonymous"></script>
	<title>Browse Products</title>
</head>

<body>
<div class="container-fluid" id="header">
	<div class="container-md">
		<nav class="navbar navbar-expand-lg">
			<div class="container">
				<a href="<?php echo base_url() ?>" class="navbar-brand text-white" id="title">
					Sweetness Home
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
						data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
						aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>BrowseProducts">Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#">About Us</a>
						</li>
					</ul>

					<ul class="navbar-nav ms-auto">
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>Login">Login</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>Registration">Sign Up</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>

<h1 class="bg-success text-white">Email Sent. Please check your inbox</h1>
</body>
</html>
