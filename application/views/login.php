<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
	<script src="https://kit.fontawesome.com/ecde828da2.js" crossorigin="anonymous"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<title>Login</title>
</head>

<body>
<div class="container-fluid" id="header">
	<div class="container-md">
		<nav class="navbar navbar-expand-lg">
			<div class="container">
				<a href="<?php echo base_url() ?>" class="navbar-brand text-white" id="title">
					Sweetness Home
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
						data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
						aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>BrowseProducts">Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#">About Us</a>
						</li>
					</ul>
					<ul class="navbar-nav ms-auto">
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>Login">Login</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>Registration">Sign Up</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>

<div class="container">

	<div class="row justify-content-center mt-5 p-3">
		<div class="col-md-8">
			<h1 id="title" class="text-center mb-3" style="display: block;">Please Login</h1>
			<form action="<?php echo base_url() ?>Login/login" method="POST">
				<span class="bg-danger text-white p-2 text-center mb-3 rounded-2"
					  style="display: <?= $display ?>;"><?= $error ?></span>
				<div class="form-floating mb-3">
					<input class="form-control" name="email" autocomplete="email" aria-required="true"
						   placeholder="name@example.com"/>
					<label class="form-label">Email</label>
					<span class="text-danger"><?php echo form_error('email') ?></span>
				</div>
				<div class="form-floating mb-3">
					<input type="password" name="password" class="form-control" autocomplete="current-password"
						   aria-required="true" placeholder="password"/>
					<label class="form-label">Password</label>
					<span class="text-danger"><?php echo form_error('password') ?></span>
				</div>
				<div class="mb-3">
					<input type="submit" value="Login" class="btn btn-outline-primary">
					<input type="reset" value="Clear" class="btn btn-outline-danger">
				</div>
			</form>
		</div>
	</div>
</div>
</body>

</html>
