<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
	<script src="https://kit.fontawesome.com/ecde828da2.js" crossorigin="anonymous"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<title>Registration</title>
</head>

<body>

<div class="container-fluid" id="header">
	<div class="container-md">
		<nav class="navbar navbar-expand-lg">
			<div class="container">
				<a href="<?php echo base_url() ?>" class="navbar-brand text-white" id="title">
					Sweetness Home
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
						data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
						aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>BrowseProducts">Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="#">About Us</a>
						</li>
					</ul>

					<ul class="navbar-nav ms-auto">
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php ?>Login">Login</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="<?php echo base_url() ?>Registration">Sign Up</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>

<div class="container">

	<div class="row justify-content-center mt-5 p-3">
		<div class="col-md-8">
			<h1 id="title" class="text-center mb-3" style="display: block;">Register Here</h1>
			<form action="<?php echo base_url() ?>Registration/register" method="POST">
				<span class="bg-danger text-center mb-3 p-3 rounded-1 text-white"
					  style="display: <?php echo $display ?>"><?php echo $error; ?></span>
				<div class="form-floating mb-3">
					<input class="form-control" name="firstname" autocomplete="firstname" aria-required="true"
						   placeholder="Jane" value="<?php echo set_value('firstname') ?>"/>
					<label class="form-label">First Name</label>
					<div class="form-text text-danger"><?php echo form_error('firstname') ?></div>
				</div>

				<div class="form-floating mb-3">
					<input class="form-control" name="lastname" autocomplete="lastname" aria-required="true"
						   placeholder="Doe" value="<?php echo set_value('lastname') ?>"/>
					<label class="form-label">Last Name</label>
					<div class="form-text text-danger"><?php echo form_error('lastname') ?></div>
				</div>

				<div class="form-floating mb-3">
					<input class="form-control" name="address" autocomplete="address" aria-required="true"
						   placeholder="address" value="<?php echo set_value('address') ?>"/>
					<label class="form-label">Address</label>
					<div class="form-text text-danger"><?php echo form_error('address') ?></div>
				</div>

				<div class="form-floating mb-3">
					<input class="form-control" type="tel" name="phonenumber" autocomplete="phonenumber"
						   aria-required="true" placeholder="phone number"
						   value="<?php echo set_value('phonenumber') ?>"/>
					<label class="form-label">Phone Number</label>
					<div class="form-text text-danger"><?php echo form_error('phonenumber') ?></div>
				</div>


				<div class="form-floating mb-3">
					<input type="date" name="birthdate" class="form-control" autocomplete="birthdate"
						   aria-required="true" value="<?php echo set_value('birthdate') ?>"/>
					<label class="form-label">Birth Date</label>
					<div class="form-text text-danger"><?php echo form_error('birthdate') ?></div>
				</div>

				<div class="form-floating mb-3">
					<input class="form-control" name="email" autocomplete="username" aria-required="true"
						   placeholder="name@example.com" value="<?php echo set_value('email') ?>"/>
					<label class="form-label">Email</label>
					<div class="form-text text-danger"><?php echo form_error('email') ?></div>
				</div>
				<div class="form-floating mb-3">
					<input type="password" name="password" class="form-control" autocomplete="current-password"
						   aria-required="true" placeholder="password"/>
					<label class="form-label">Password</label>
					<div class="form-text text-danger"><?php echo form_error('password') ?></div>
				</div>
				<div class="form-floating mb-3">
					<input type="password" name="confirmpassword" class="form-control" autocomplete="confirm-password"
						   aria-required="true" placeholder="Confirm Password"/>
					<label class="form-label">Confirm Password</label>
					<div class="form-text text-danger"><?php echo form_error('confirmpassword') ?></div>
				</div>
				<div class="mb-3">
					<input type="submit" value="Register" class="btn btn-outline-primary">
					<input type="reset" value="Clear" class="btn btn-outline-danger">
				</div>
			</form>

		</div>

	</div>

</div>


</body>

</html>
